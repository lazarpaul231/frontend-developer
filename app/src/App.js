import React from 'react';
import Table from './Table/Table';
import './App.scss';

function App() {
  return (
    <div className="app">
      <Table />
    </div>
  );
}

export default App;