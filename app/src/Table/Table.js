import React, { useEffect, useState } from 'react';
import TableRow from './TableRow';

function Table() {
  const [data, setData] = useState([]);

  useEffect(() => {
    Promise.all([
      fetch('/api/hourentries.json').then(res => res.json()),
      fetch('/api/projects.json').then(res => res.json()),
      fetch('/api/customers.json').then(res => res.json()),
    ]).then(([hourentries, projects, customers]) => {
      const hoursWorkedPerProject = hourentries.reduce((acc, entry) => {
        acc[entry.project] = (acc[entry.project] || 0) + entry.timeSpent;
        return acc;
      }, {});

      const newData = hourentries.map(entry => {
        const project = projects.find(p => p.id === entry.project);
        const customer = customers.find(c => c.id === project.customer);
        const totalHours = project.hours;
        const hoursWorked = hoursWorkedPerProject[entry.project];
        const remainingHours = totalHours - hoursWorked;
        return {
          ...entry,
          project: project.name,
          customer: customer.name,
          totalHours,
          remainingHours, 
        };
      });
      setData(newData);
    });
  }, []);

  const handleSave = (id, newData) => {
    setData(prevData => {
      let hoursWorkedPerProject = prevData.reduce((acc, entry) => {
        acc[entry.project] = (acc[entry.project] || 0) + (entry.id === id ? newData.timeSpent : entry.timeSpent);
        return acc;
      }, {});

      const updatedData = prevData.map(item => {
        const totalHours = item.totalHours;
        const hoursWorked = hoursWorkedPerProject[item.project];
        const remainingHours = totalHours - hoursWorked;
        console.log(remainingHours, 'remainingHours');
        return { ...item, timeSpent: item.id === id ? newData.timeSpent : item.timeSpent, totalHours, remainingHours };
      });
      return updatedData;
    });
  };

  return (
    <table className='app__table'>
      <thead>
        <tr>
          <th>Engineer</th>
          <th>Project</th>
          <th>Customer</th>
          <th>Allocated Hours</th>
          <th>Worked Hours</th>
          <th>Remaining Hours</th>
          <th>Issue URL</th>
          <th>Edit</th>
        </tr>
      </thead>
      <tbody>
        {data.map((row) => (
          <TableRow key={row.id} data={row} onSave={handleSave} />
        ))}
      </tbody>
    </table>
  );
}

export default Table;