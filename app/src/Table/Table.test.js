import React from 'react';
import { render, screen } from '@testing-library/react';
import Table from './Table';

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve([
      { id: 1, engineer: 'Jarvis', project: 'Calipso', customer: 'Stark Enterprises', timeSpent: '15', issueUrl: 'http://something.com' },
      { id: 2, project: 'Arkanauts', customer: 'NASA', timeSpent: '1', issueUrl: 'http://somethingelse.com' },
      { id: 3, project: 'Valhalla', customer: 'Vikings', timeSpent: '33', issueUrl: 'http://idonthaveideea.com' },
      { id: 4, engineer: 'Bob', project: 'Project X', customer: 'Netflix', timeSpent: '0', issueUrl: 'http://test.com' },
      { id: 5, project: 'Goa', customer: 'India', timeSpent: '200', issueUrl: 'http://example.com' },
    ]),
  })
);

test('renders the table', async () => {
  render(<Table />);

  const table = await screen.findByRole('table');
  expect(table).toBeInTheDocument();
});