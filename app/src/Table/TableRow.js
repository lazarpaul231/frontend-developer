import React, { useState, useEffect } from 'react';

function TableRow({ data, onSave }) {
  const [isEditing, setIsEditing] = useState(false);
  const [newData, setNewData] = useState({ ...data });
  const [notification, setNotification] = useState(null);

  useEffect(() => {
    setNewData({ ...data });
  }, [data]);

  const handleEdit = () => {
    setIsEditing(true);
  };

  const showNotification = (message, type) => {
    setNotification({ message, type });
    setTimeout(() => {
      setNotification(null);
    }, 2000);
  };

  const handleSave = () => {
    if (newData.timeSpent === data.timeSpent) {
      setIsEditing(false);
      showNotification('No changes were made to the hours', 'error');
    } else {
      onSave(data.id, newData);
      setIsEditing(false);
      showNotification('The modification has been successfully completed', 'success');
    }
  };

  const handleChange = (e) => {
    const value = e.target.name === 'timeSpent' ? Number(e.target.value) : e.target.value;
    setNewData({
      ...newData,
      [e.target.name]: value,
    });
  };

  return (
    <>
      {notification && <div className={`notification notification_${notification.type}`}>{notification.message}</div>}
      <tr>
        <td>{data.engineer || 'unknown'}</td>
        <td>{newData.project}</td>
        <td>{newData.customer}</td>
        <td>{newData.totalHours}</td>
        <td>{isEditing ? <input name='timeSpent' value={newData.timeSpent} onChange={handleChange} /> : newData.timeSpent}</td>
        <td>{newData.remainingHours}</td>
        <td><a href={newData.issueUrl} target='blank'>URL</a></td>
        <td>
          {isEditing ? (
            <button onClick={handleSave}>Save</button>
          ) : (
            <button onClick={handleEdit}>Edit</button>
          )}
        </td>
      </tr>
    </>
  );
}

export default TableRow;