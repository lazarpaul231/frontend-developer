
### STARTING THE REACT APP


## Prerequisites

Before you can start the application, you need to have the following installed:

Node.js: You can download it from https://nodejs.org/en/download/
Yarn: After installing Node.js, you can install Yarn by running npm install -g yarn in your terminal.


## Clone the Repository

First, you need to clone the repository to your local machine. You can do this by running the following command in your terminal:

`git clone https://gitlab.com/lazarpaul231/frontend-developer.git`


## Install Dependencies

Navigate into the project directory:

`cd app`

Then, install the project dependencies:

`yarn install`

This command installs all the dependencies defined in the package.json file.


## Start the Application

`yarn start`

This command starts the application in development mode. Open http://localhost:3000 to view it in the browser. The page will reload if you make edits.


### RUNNING THE TESTS

`yarn test`

This command runs the test watcher (Jest). By default, runs tests related to files changed since the last commit.

Unfortunately, this functionality does not work as it should, and the tests fail, because of an error that I did not manage to solve in time and I just delved too much into stackoverflow